﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HalconDotNet;
using RD61.CommonData;
using RD61.PLC;
using RD61.Base;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Windows.Threading;

namespace RD61.Base
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
            ILog Log;
            HWindow LabelWindow;
            HWindow VerificationWindow;
            LabelCameraData LabelData;
            VerificationCameraData VerificationData;
            PlcCommsManager plc;
            CameraManager CamManager;
            public MainWindow()
            {
                InitializeComponent();
                Config AppConfig = Config.Instance;
                Log = new Log("GUI");
                //Create Camera Manager to Connect Cameras
                //var CamManager = CameraManager.Instance;
                var ParCamManager = CameraManagerParallel.Instance;
                CameraManagerParallel.Instance.PropertyChanged += new PropertyChangedEventHandler(OnCameraPropertyChanged);
                //Create PLC
                Log.I("Connecting to the PLC");
                var plc = PlcCommsManager.Instance;
                //Create Local Data Objects
                LabelData = new LabelCameraData();
                VerificationData = new VerificationCameraData();
                //Subcribe to PLC Properties and Start Plc Comms
                plc.PropertyChanged += new PropertyChangedEventHandler(OnPlcPropertyChanged);
                plc.InitiatePlcComms("10.100.100.2.1.1", 851, 5);//Debug PLC String
                //plc.InitiatePlcComms(AppConfig.AmsID,AppConfig.AmsPort,AppConfig.PlcReconnectTime);
             }

            #region PLC Notification
            public void OnPlcPropertyChanged(object sender, PropertyChangedEventArgs e)
            {
                if (e.PropertyName == "PlcRunning")
                {
                    if (PlcCommsManager.Instance.PlcRunning)
                    {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            radioButtonPLCConnected.IsChecked = true;
                        }));
                    
                        Log.I($"PLC Connected");
                    }
                    else
                    {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            radioButtonPLCConnected.IsChecked = false;
                        }));
                        Log.I($"PLC Disconnected");
                    }

                }
            }
        #endregion
        #region Camera Notification
        public void OnCameraPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "LabelData")
            {
                try
                {
                    LabelData = CameraManagerParallel.Instance.LabelData;

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            LabelWindow.DispImage(LabelData.LabelImage);
                        }));
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            labelTextBoxXCoord.Text = Convert.ToString(LabelData.x);
                        }));
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            labelTextBoxYCoord.Text = Convert.ToString(LabelData.y);
                        }));
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            Q1textBox.Text = Convert.ToString(LabelData.Quat[0].D);
                        }));
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            Q2textBox.Text = Convert.ToString(LabelData.Quat[1].D);
                        }));
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            Q3textBox.Text = Convert.ToString(LabelData.Quat[2].D);
                        }));
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            Q4textBox.Text = Convert.ToString(LabelData.Quat[3].D);
                        }));

                }
                catch(Exception ex)
                {
                    Log.E($"Exception Thrown Updating UI in OnCamPropertiesChanged: {ex.Message}");
                }



            }
            if (e.PropertyName == "VerificationData")
            {
                try
                {
                    //Save Data Locally to only enter Lock once
                    VerificationData = CameraManagerParallel.Instance.VerificationData;
                    //Dispatch to unpadte UI
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            VerificationWindow.DispImage(VerificationData.VerificationImage);
                        }));
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            VerificationTextBoxBarcode.Text = VerificationData.Barcode;
                        }));
                }
                catch (Exception ex)
                {
                    Log.E($"Exception ThrownUpdating UI in OnCamPropertiesChanged: {ex.Message}");
                }
            }
        }
        #endregion
        #region User Control Events
        /// <summary>
        /// Required to get the handle of a Halcon Smart Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowRight_Loaded(object sender, RoutedEventArgs e)
        {
            LabelWindow = WindowRight.HalconWindow;
        }
        /// <summary>
        /// Required to get the handle of a Halcon Smart Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowLeft_Loaded(object sender, RoutedEventArgs e)
        {
            VerificationWindow = WindowLeft.HalconWindow;
        }
        #endregion
        #region Private UI Methods
        private void DisplayImageHWindow(HWindow Window, HImage ImagetoDisplay)
            {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                new Action(() =>
                {
                    try
                    {
                        HTuple _pointer = new HTuple();
                        HTuple _type = new HTuple();
                        HTuple _width = new HTuple();
                        HTuple _height = new HTuple();
                        HTuple _zero = 0;
                        HOperatorSet.GetImagePointer1(ImagetoDisplay, out _pointer, out _type, out _width, out _height);
                        Window.SetPart(_zero, _zero, _height - 1, _width - 1);
                        Window.DispImage(ImagetoDisplay);
                    }
                    catch (HalconException hex)
                    {
                        Log.E($"Exception Thrown by DisplayImageHWindow, Window:{Window}, Message:{hex.Message}");
                    }
                    catch(Exception ex)
                    {
                        Log.E($"Exception Thrown by Display ImageHWindow, {ex.Message}");
                    }
                    
                }));

            }
            #endregion


        }
    }

