﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;

namespace RD61.Base
{
    class Config
    {
        public static Config Instance { get; } = new Config();
        private string _verificationCameraID;
        
        public string VerificationCameraID
        {
            get { return _verificationCameraID; }
        }

        private string _labelCameraID;

        public string LabelCameraID
        {
            get { return _labelCameraID; }
        }

        private string _labelProcedure;

        public string LabelProcedure
        {
            get { return _labelProcedure; }
        }

        private string _verificationProcedure;

        public string VerificationProcedure
        {
            get { return _verificationProcedure; }
        }

        private string _amsID;

        public string AmsID
        {
            get { return _amsID; }
        }
        private int _amsport;

        public int AmsPort
        {
            get { return _amsport; }
            
        }
        private int _plcReconnectTime;

        public int PlcReconnectTime
        {
            get { return _plcReconnectTime; }
            
        }
        private HTuple _cameraPose;

        public  HTuple CameraPose
        {
            get { return _cameraPose; }
            set { _cameraPose = value; }
        }

        private HTuple _cameraParameters;

        public HTuple CameraParameters
        {
            get { return _cameraParameters; }
            set { _cameraParameters = value; }
        }

    
        public Config()
        {
            RetriveAppConfig();
        }
        public bool RetriveAppConfig()
        {
            try
            {
                _labelCameraID = ConfigurationManager.AppSettings.Get("LabelCameraID");
                _verificationCameraID = ConfigurationManager.AppSettings.Get("VerificationCameraID");
                _labelProcedure = ConfigurationManager.AppSettings.Get("LabelProcedure");
                _verificationProcedure = ConfigurationManager.AppSettings.Get("VerificationProcedure");
                _amsID = ConfigurationManager.AppSettings.Get("AmsID");
                _amsport = Convert.ToInt32(ConfigurationManager.AppSettings.Get("AmsPort"));
                _plcReconnectTime = Convert.ToInt32(ConfigurationManager.AppSettings.Get("PlcReconnectTime"));
                _cameraPose = new HPose();
                HOperatorSet.ReadPose(ConfigurationManager.AppSettings.Get("CameraPose"), out _cameraPose);
                
                _cameraParameters = new HCamPar();
                HOperatorSet.ReadCamPar(ConfigurationManager.AppSettings.Get("CameraParameters"), out _cameraParameters);
                //HOperatorSet.TupleSelectRange(_cameraParameters, 1, 8, out _cameraParameters);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
