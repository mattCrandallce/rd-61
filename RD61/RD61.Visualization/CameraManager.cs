﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using HalconDotNet;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using RD61.PLC;
using RD61.CommonData;



namespace RD61.Base
{
    class CameraManager : INotifyPropertyChanged, IDisposable
    {
        public static CameraManager Instance { get; } = new CameraManager();

        
        #region Properties
        public string LabelCameraID { get; set; }
        public string VerificationCameraID { get; set; }
        public string LabelProcedure { get; set; }
        public string VerificationProcedure { get; set; }
        public int MyProperty { get; set; }
        public LabelCameraData LabelData { get; set; }
        public VerificationCameraData VerificationData { get; set; }
        public string AmsID { get; set; }
        public int AmsPort { get; set; }
        public int PlcReconnetTime { get; set; }
        private bool _startCameraInspection;

        public bool StartCameraInspection
        {
            get { return _startCameraInspection; }
            set
            {
                if (_startCameraInspection == value) return;
                _startCameraInspection = value;
                OnPropertyChanged();
            }
        }


        #endregion
        #region Local Private Variables
        private HDevEngine Engine;
        private HDevProcedure hLabelProcedure;
        private HDevProcedure hVerificationProcedure;
        private HDevProcedureCall mLabelProcedureCall;
        private HDevProcedureCall mVerificationProcedureCall;
        private HFramegrabber hFrameGrabberLabelCamera;
        private HFramegrabber hFrameGrabberVerificationCamera;
        protected ILog Log;
        #endregion
        #region Public Methods
        /// <summary>
        /// Constructor
        /// </summary>
        CameraManager()
        {
            Log = new Log("CameraManager");
            try
            {
                Config AppConfig = new Config();
                LabelCameraID = AppConfig.LabelCameraID;
                VerificationCameraID = AppConfig.VerificationCameraID;
                LabelProcedure = AppConfig.LabelProcedure;
                VerificationProcedure = AppConfig.VerificationProcedure;
                AmsID = AppConfig.AmsID;
                AmsPort = AppConfig.AmsPort;
                PlcReconnetTime = AppConfig.PlcReconnectTime;
                try
                {
                    EngineConstructor();
                    ConnectCameras();
                    PlcCommsManager.Instance.PropertyChanged += new PropertyChangedEventHandler(OnCamNotification);
                    VerificationCameraData VerificationData = new VerificationCameraData();
                    LabelCameraData LabelData = new LabelCameraData();
                    
                }
                catch (Exception ex)
                {
                    Log.E($"Exception message from: {ex.Message}");
                }
            }
            catch (NullReferenceException nullex)
            {
                Log.E($"Config failed to extract Parameters exception message: {nullex.Message}");
            }
            catch (Exception ex)
            {
                Log.E($"Exception message from CameraManager Constructor:{ex.Message}");
            }
           
               
            
            
        }


        #endregion
        #region Private Methods
        /// <summary>
        /// Private Method that is called when the plc flag is true is notified by onPlcPropertyChange 
        /// The method will store the results in public properties within camera manager
        /// Procedure names are pulled from the app config values the procedure path is with the app folder
        /// </summary>
        private void RunProceedures()
        {
            try
            {
                runVerificationProcedure();
                runLabelProcedure();
            }
            catch (HalconException hex)
            {
                Log.E($"Error Occured in Halcon Execution Halcon Exception Message:{hex.Message}");
            }
            catch (Exception ex)
            {
                Log.E($"Exception From RunProcedures in CameraManager Message: {ex}");
            }
        }
        /// <summary>
        /// Method to call that will connect both of the cameras based on the appconfig Ids 
        /// </summary>
        private void ConnectCameras()
        {
            try
            {
                //Two Methods
                hFrameGrabberLabelCamera = new HFramegrabber("GigEVision", 0, 0, 0, 0, 0, 0, "default", -1, "default", -1, "False",
                                                                            "default", $"{LabelCameraID}", 0, -1);
                hFrameGrabberVerificationCamera = new HFramegrabber("GigEVision", 0, 0, 0, 0, 0, 0, "default", -1, "default", -1, "False",
                                                                            "default", $"{VerificationCameraID}", 0, -1);
            }
            catch (HalconException hex)
            {
                Log.E($"Error in Halcon Execution From ConnectCameras Method, {hex.Message}");
            }
            catch (Exception ex)
            {
                Log.E($"Exception Thrown by ConncetCameras Method, {ex.Message}");
            }
        }
        /// <summary>
        /// Private Method to Create an instance of HdevEngine and set the procedure path
        /// </summary>
        private void EngineConstructor()
        {
            try
            {
                Engine = new HDevEngine();
                Engine.SetProcedurePath("C:\\Users\\matt\\Documents\\Visual Studio 2015\\Projects\\RD61\\RD61\\Halcon Procedures");
                hVerificationProcedure = new HDevProcedure(VerificationProcedure);
                hLabelProcedure = new HDevProcedure(LabelProcedure);
                mLabelProcedureCall = new HDevProcedureCall(hLabelProcedure);
                mVerificationProcedureCall = new HDevProcedureCall(hVerificationProcedure);
            }
            catch(HDevEngineException hengex)
            {
                Log.E($"Error in Creating the HdevEngine Exception Message:{hengex.Message}");
            }
            catch (Exception ex)
            {
                Log.E($"Eception Throw by EngineConstructor in CameraManager Exception Message: {ex.Message}");
            }
        }
        /// <summary>
        /// Private Method sets the inputs of the label prcedure runs the procedure and then stores the outputs in the label camera data class
        /// </summary>
        private void runLabelProcedure()
        {
            try
            {
                //Grab Image
                LabelData.LabelImage = GrabImage(hFrameGrabberLabelCamera);
                //Set Inputs
                mLabelProcedureCall.SetInputIconicParamObject("Image", LabelData.LabelImage);
                //Run Procedure
                mLabelProcedureCall.Execute();
                //Convert from tuple to integer and assign to class variables
                LabelData.x = mLabelProcedureCall.GetOutputCtrlParamTuple("Outputx").I;
                LabelData.y = mLabelProcedureCall.GetOutputCtrlParamTuple("Outputy").I;
            }
            catch (HDevEngineException hengex)
            {
                Log.E($"An Error Occured With the HdevEngine Error Message:{hengex.Message}");
            }
            catch (HalconException hex)
            {
                Log.E($"Error Occured in Haclon Execution Erro Message:{hex.Message}");
            }
            catch (Exception ex)
            {
                Log.E($"General Exception Occured in runLabelProcedure Message:{ex.Message}");
            }
        }
        /// <summary>
        /// Private Method sets the inputs of the verification Procedure runs the Procedure and then stores the outputs in the verification camera data class
        /// </summary>
        private void runVerificationProcedure()
        {
            try
            {
                //Grab Image
                VerificationData.VerificationImage = GrabImage(hFrameGrabberVerificationCamera);
                //set Inputs
                mVerificationProcedureCall.SetInputIconicParamObject("Image", VerificationData.VerificationImage);
                //Run Procedure
                mVerificationProcedureCall.Execute();
                //Convert from tuple to string and assign to class variables
                VerificationData.Barcode = mVerificationProcedureCall.GetOutputCtrlParamTuple("Barcode").S;
            }
            catch (HalconException hex)
            {
                Log.E($"Error in Halcon Execution from runVerificationProcedure Method, {hex.Message}");
            }
            catch (Exception ex)
            {
                Log.E($"Exception Thrown in runVerificationProcedure method, {ex.Message}");
            }
        }/// <summary>
         /// Private Method to grab an image from any passed frame grabber
         /// </summary>
         /// <param name="FrameGrabber"></param>
         /// <returns></returns>
        private HImage GrabImage(HFramegrabber FrameGrabber)
        {
            HImage Image = new HImage();
            try
            {
                Image = FrameGrabber.GrabImage();
                return Image;
            }
            catch(HalconException hex)
            {
                Log.E($"Error in Halcon Execution in GrabImage Method, {hex.Message}");
                return Image;
            }
            catch (Exception ex)
            {
                Log.E($"exception Thrown in GrabImage method, {ex.Message}");
                return Image;
            }
        }
        #endregion
        #region Propertry Changed 
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private void OnCamNotification(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "_StartCameraInspection")
            {
                StartCameraInspection = PlcCommsManager.Instance.StartCameraInspection;
                if (StartCameraInspection)
                {
                    //Run Procedures and then call the plcomms method to write the results over ads
                    //TODO: Two PLC Flags
                    RunProceedures();
                    //Plc Comms method to Write camera results over ads 
                    //also write plc version of StartCameraInspection False
                    PlcCommsManager.Instance.WriteCameraResults(LabelData, VerificationData);
                }
                
            }

        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    hFrameGrabberLabelCamera.Dispose();
                    hFrameGrabberVerificationCamera.Dispose();
                }

               

                disposedValue = true;
            }
        }

        
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            
        }
        #endregion
        #endregion
    }
}
