﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;
using RD61.CommonData;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using RD61.PLC;
using System.Windows.Threading;
using System.Windows;

namespace RD61.Base
{
    public class CameraManagerParallel:INotifyPropertyChanged, IDisposable
    {
        public static CameraManagerParallel Instance { get; } = new CameraManagerParallel();
        #region Properties
        public string LabelCameraID { get; set; }
        public string VerificationCameraID { get; set; }
        public string LabelProcedure { get; set; }
        public string VerificationProcedure { get; set; }
        public int MyProperty { get; set; }
        private ReaderWriterLockSlim _verificationlock = new ReaderWriterLockSlim();
        private VerificationCameraData _verificationData;

        public VerificationCameraData VerificationData
        {
            get { return ReadWriteLockSlimHelper<VerificationCameraData>.GetReadWriteLock(_verificationlock, _verificationData); }
            set
            {
                ReadWriteLockSlimHelper<VerificationCameraData>.SetReadWriteLock(_verificationlock, ref _verificationData, value);
                OnPropertyChanged();
            }
        }


        private LabelCameraData _labelData;
        private static ReaderWriterLockSlim _labellock = new ReaderWriterLockSlim();
        public LabelCameraData LabelData
        {
            get
            { return ReadWriteLockSlimHelper<LabelCameraData>.GetReadWriteLock(_labellock, _labelData); }
            set
            {
                ReadWriteLockSlimHelper<LabelCameraData>.SetReadWriteLock(_labellock, ref _labelData, value);
                OnPropertyChanged();
            }
        }
        private bool _imageGrabbing;
        private static ReaderWriterLockSlim _imageGrabbingLock = new ReaderWriterLockSlim();
        public bool ImageGrabbing
        {
            get
            { return ReadWriteLockSlimHelper<bool>.GetReadWriteLock(_labellock, _imageGrabbing); }
            set
            {
                ReadWriteLockSlimHelper<bool>.SetReadWriteLock(_labellock, ref _imageGrabbing, value);
                OnPropertyChanged();
            }
        }
        //private LabelCameraData _labelData;
        //public LabelCameraData LabelData
        //{
        //    get { return _labelData; }
        //    set
        //    {
        //        if (_labelData != value)
        //        {
        //            _labelData = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private VerificationCameraData _verificationData;

        //public VerificationCameraData VerificationData
        //{
        //    get { return _verificationData; }
        //    set
        //    {
        //        if(_verificationData != value)
        //        {
        //            _verificationData = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}
        public bool LabelCameraOnline { get; set; }
        public bool VerificationCameraOnline { get; set; }
        public string AmsID { get; set; }
        public int AmsPort { get; set; }
        public int PlcReconnetTime { get; set; }
        private bool _startCameraInspection;

        public bool StartCameraInspection
        {
            get { return _startCameraInspection; }
            set
            {
                if (_startCameraInspection == value) return;
                _startCameraInspection = value;
                OnPropertyChanged();
            }
        }
        private bool _startVerificationCamerInspection;

        public bool StartVerificationCameraInspection
        {
            get { return _startVerificationCamerInspection; }
            set
            {
                if (_startVerificationCamerInspection == value) return;
                _startVerificationCamerInspection = value;
                OnPropertyChanged();
            }
        }

        private bool _startLabelCamerInspection;

        public bool StartLabelCameraInspection
        {
            get { return _startLabelCamerInspection; }
            set
            {
                if (_startLabelCamerInspection == value) return;
                _startLabelCamerInspection = value;
                OnPropertyChanged();
            }
        }
        private bool _labelComplete;

        public bool LabelComplete
        {
            get { return _labelComplete; }
            set
            {
                if (_labelComplete != value)
                {
                    _labelComplete = value;
                }
                    ;
            }
        }

        private bool _verificationComplete;

        public bool VerificationComplete
        {
            get { return _verificationComplete; }
            set
            { if (_verificationComplete != value)
                {
                    _verificationComplete = value;
                };
            }
        }


        #endregion
        #region Local Private Variables
        private HDevEngine Engine;
        private HDevProcedure hLabelProcedure;
        private HDevProcedure hVerificationProcedure;
        private HDevProcedureCall mLabelProcedureCall;
        private HDevProcedureCall mVerificationProcedureCall;
        private HFramegrabber hFrameGrabberLabelCamera;
        private HFramegrabber hFrameGrabberVerificationCamera;
        
        protected ILog Log;
        #endregion
        #region Public Methods
        /// <summary>
        /// Constructor Create Engine and Pull in App Config Values
        /// </summary>
        public CameraManagerParallel()
        {
            Log = new Log("CameraManager");
            try
            {
                
                LabelCameraID = Config.Instance.LabelCameraID;
                VerificationCameraID = Config.Instance.VerificationCameraID;
                LabelProcedure = Config.Instance.LabelProcedure;
                VerificationProcedure = Config.Instance.VerificationProcedure;
                
                try
                {
                    
                    startLabelCamera();
                    startVerificationCamera();
                    PlcCommsManager.Instance.PropertyChanged += new PropertyChangedEventHandler(OnPlcPropertyChanged);
                    _verificationData = new VerificationCameraData();
                    _labelData = new LabelCameraData();
                    
                    engineConstructor();

                }
                catch (Exception ex)
                {
                    Log.E($"Exception message from CarmeraManagerParallel Method, {ex.Message}");
                }
            }
            catch (NullReferenceException nullex)
            {
                Log.E($"Config failed to extract Parameters exception message, {nullex.Message}");
            }
            catch (Exception ex)
            {
                Log.E($"Exception message from CameraManager Constructor, {ex.Message}");
            }
        }
        public void stopLabelCamera()
        {
            try
            {
                StopLabelCameraTask();
            }
            catch (Exception ex)
            {
                Log.E($"Exception Thrown by StopLabelCamera Method, {ex.Message}");
                
            }
            
        }
        public void stopVerificationCamera()
        {
            try
            {
                stopVerificationCameraTask();
            }
            catch (Exception ex)
            {
                Log.E($"Exception Thrown by StopVerificationCamera Method, {ex.Message}");
                
            }
        }
        public void stopBothCameras()
        {
            try
            {
                stopVerificationCameraTask();
                StopLabelCameraTask();
            }
            catch (Exception ex)
            {
                Log.E($"Exception Thrown by stopBothCameras Method, {ex.Message}");
            }
        }
        #endregion
        #region Private Methods
        #region LabelCamera
        private CancellationTokenSource _labelTokenSource;
        private CancellationToken _labelToken;
        private Task _labelCameraTask;
        private bool _labelImageGrabbed;
        private void startLabelCamera()
        {
            try
            {
                _labelTokenSource = new CancellationTokenSource();
                _labelToken = _labelTokenSource.Token;
                _labelCameraTask = new Task(() => labelCameraTask(), _labelToken, TaskCreationOptions.LongRunning);
                _labelCameraTask.Start();
            }
            catch (Exception ex)
            {
                Log.E($"General Exception, startLabelCamera Method: {ex.Message}");
            }
        }
        private void labelCameraTask()
        {
            try
            {
                //Connect to camera
                try
                {
                    Log.I($"Label Task Started");
                    
                    hFrameGrabberLabelCamera = new HFramegrabber("GigEVision", 0, 0, 0, 0, 0, 0, "default", -1, "default", -1, "false",
                                                                                    "default", $"{LabelCameraID}", 0, -1);
                    //hFrameGrabberLabelCamera = new HFramegrabber("File", 0, 0, 0, 0, 0, 0, "default", -1, "default", -1, "false",
                                                                            //"C:/Users/matt/Pictures/ParallelTrain", "default", 0, 0);
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            LabelCameraOnline = true;
                        }));
                    
                    Log.I($"Label Task Frame Grabber Created");
                }
                catch(HalconException hex)
                {
                    Log.E($"Halcon Exception, labelCameraTask Method: {hex.Message}");
                }
                //Set all camera Parameters
                //hFrameGrabberLabelCamera.SetFramegrabberParam("exposure",26000);
                //New Parameters
                while (true)
                {
                    if (_labelToken.IsCancellationRequested || _startLabelCamerInspection)
                    {
                        if (_labelToken.IsCancellationRequested)
                        {
                            Log.I($"Label Task Cancellation Requested");
                            hFrameGrabberLabelCamera.Dispose();
                            throw new OperationCanceledException(_labelToken);
                        }
                        if(!LabelComplete)
                        {
                            try
                            {
                                Log.I($"Label Task Work Starting");
                                LabelCameraData _labelCamaeraTaskData = new LabelCameraData();
                                if (!ImageGrabbing)
                                {
                                    ImageGrabbing = true;
                                    Log.I($"Label Image Grabbing");
                                    _labelCamaeraTaskData.LabelImage = hFrameGrabberLabelCamera.GrabImage();
                                    _labelImageGrabbed = true;
                                    ImageGrabbing = false;
                                    
                                }
                                if (_labelImageGrabbed)
                                {
                                    //set inputs
                                    mLabelProcedureCall.SetInputIconicParamObject("i_Image", _labelCamaeraTaskData.LabelImage);
                                    mLabelProcedureCall.SetInputCtrlParamTuple("i_CameraParameters", Config.Instance.CameraParameters);
                                    mLabelProcedureCall.SetInputCtrlParamTuple("i_CameraPose", Config.Instance.CameraPose);
                                    //Run Procedure
                                    Log.I($"Running Label Procedure");
                                    mLabelProcedureCall.Execute();
                                    //Outputs
                                    _labelCamaeraTaskData.Quat = mLabelProcedureCall.GetOutputCtrlParamTuple("q_Phi");
                                    _labelCamaeraTaskData.boxpresent = mLabelProcedureCall.GetOutputCtrlParamTuple("q_boxpresent");
                                    _labelCamaeraTaskData.LabelOrientation = mLabelProcedureCall.GetOutputCtrlParamTuple("q_LabelOrientation");
                                    _labelCamaeraTaskData.x = mLabelProcedureCall.GetOutputCtrlParamTuple("q_X");
                                    _labelCamaeraTaskData.y = mLabelProcedureCall.GetOutputCtrlParamTuple("q_Y");
                                    Log.I($"Label Task Data Ready X:{_labelCamaeraTaskData.x} , Y:{_labelCamaeraTaskData.y}, LabelOrientation:{_labelCamaeraTaskData.LabelOrientation}");
                                    

                                    //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => { Instance.LabelData = _labelCamaeraTaskData; }));
                                    LabelData = _labelCamaeraTaskData;
                                    LabelComplete = true;
                                    Log.I($"Label Task Work Complete");
                                    PlcCommsManager.Instance.WriteCameraResultsLabel(_labelCamaeraTaskData);
                                    _labelImageGrabbed = false;
                                    GC.Collect();
                                    GC.WaitForPendingFinalizers();
                                }
                            }
                            catch (HalconException hex)
                            {
                                Log.E($"Halcon Exception, labelCameraTask Method: {hex.Message}");
                                ImageGrabbing = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.E($"General Exception, labelCameraTask Method: {ex.Message}");
            }
        }
        private void StopLabelCameraTask()
        {
            _labelTokenSource?.Cancel();
            try
            {
                _labelCameraTask.Wait();
            }
            catch (AggregateException ae)
            {
                if(ae.InnerException is TaskCanceledException)
                {
                    Log.I($"Label Task Cencelled by CameraManagerParallel");
                }
               
            }
            catch(Exception ex)
            {
                Log.E($"Exception Thrown in StopLabelCameraTask: {ex.Message}");
            }
            finally
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            LabelCameraOnline = false;
                        }));
            }
        }
        #endregion
        #region Verification Camera
        private CancellationTokenSource _verificationTokenSource;
        private CancellationToken _verificationToken;
        private Task _verificationCameraTask;
        private bool _verificationImageGrabbed;
        private void startVerificationCamera()
        {
            try
            {
                _verificationTokenSource = new CancellationTokenSource();
                _verificationToken = _verificationTokenSource.Token;
                _verificationCameraTask = new Task(() => verificationCameraTask(), _verificationToken, TaskCreationOptions.LongRunning);
                _verificationCameraTask.Start();
            }
            catch (Exception ex)
            {
                Log.E($"General Exception, startVerification Method: {ex.Message}");
            }
        }
        private void verificationCameraTask()
        {
            //Open Frame Grabber
            try
            {
                Log.I($"Verification Task Started");
                hFrameGrabberVerificationCamera = new HFramegrabber("GigEVision", 0, 0, 0, 0, 0, 0, "default", -1, "default", -1, "false",
                "default", $"{VerificationCameraID}", 0, -1);
                //hFrameGrabberVerificationCamera = new HFramegrabber("File", 0, 0, 0, 0, 0, 0, "default", -1, "default", -1, "false", 
                                                                        //"C:/Users/matt/Pictures/ParallelTrain", "default", 0, 0);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            VerificationCameraOnline = true;
                        }));
                Log.I($"Verification Task Frame Grabber Created");
                //Set Frame Grabber Parameters
            }
            catch (HalconException hex)
            {
                Log.E($"Halcon Exception, verificationCameraTask: {hex.Message}");
            }
            

            //Task
            while (true)
            {
                
                if(_verificationToken.IsCancellationRequested || _startVerificationCamerInspection)
                {
                    if (_verificationToken.IsCancellationRequested)
                    {
                        Log.I($"Verification Task Cancellation Requested");
                        hFrameGrabberVerificationCamera.Dispose();
                        throw new OperationCanceledException(_verificationToken);
                        
                    }
                    if(!VerificationComplete)
                    {
                        try
                        {
                            Log.I($"Verification Task Work Started");
                            VerificationCameraData _verificationTaskData = new VerificationCameraData();
                            if (!ImageGrabbing)
                            {
                                ImageGrabbing = true;
                                //Grab Image
                                Log.I($"Grabbing Verification Image");
                                _verificationTaskData.VerificationImage = hFrameGrabberVerificationCamera.GrabImage();
                                _verificationImageGrabbed = true;
                                ImageGrabbing = false;
                               
                            }
                            if (_verificationImageGrabbed)
                            {
                                //set inputs
                                mVerificationProcedureCall.SetInputIconicParamObject("Image", _verificationTaskData.VerificationImage);
                                //execute 
                                mVerificationProcedureCall.Execute();
                                //Get Outputs
                                _verificationTaskData.Barcode = mVerificationProcedureCall.GetOutputCtrlParamTuple("Barcode").S;
                                Log.I($"Verification Task Data Ready Barcode: {_verificationTaskData.Barcode}");
                                //Set Outputs
                                Log.I($"Verification Task Work Finished");
                                VerificationData = _verificationTaskData;
                                VerificationComplete = true;
                                PlcCommsManager.Instance.WriteCameraResultsVerification(_verificationTaskData);
                                //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => { CameraManagerParallel.Instance.VerificationData = _verificationTaskData; }));                           
                                _verificationImageGrabbed = false;
                                GC.Collect();
                                GC.WaitForPendingFinalizers();
                            }
                        }
                        catch (HDevEngineException hdengex)
                        {
                            Log.E($"Halcon Engine Exception, verificationCameraTask method: {hdengex.Message}");
                        }
                        catch (HalconException hex)
                        {
                            Log.E($"Halcon Exeution Exception, verificationCameraTask method: {hex.Message}");
                        }
                        catch (Exception ex)
                        {
                            Log.E($"General Exception, verificationCameraTask method: {ex.Message}");
                        }
                        
                    }
                }
            }

        }
        private void stopVerificationCameraTask()
        {
            _verificationTokenSource?.Cancel();
            try
            {

                _verificationCameraTask.Wait();
            }
            catch (AggregateException ae)
            {
                if(ae.InnerException is TaskCanceledException)
                {
                    Log.I($"Verification Task Cancelled From CameraManagerParallel");
                }
                else
                {
                    Log.E($"Exception thrown by stopVerificationCameraTask method: {ae.Message}");
                }
            }
            finally
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() =>
                        {
                            VerificationCameraOnline = false;
                        }));
            }
        }
        #endregion
        /// <summary>
        /// Private Method to Create an instance of HdevEngine and set the procedure path
        /// </summary>
        private void engineConstructor()
        {
            try
            {
                Engine = new HDevEngine();
                Engine.SetProcedurePath("C:\\Matt\\Customers\\RX\\RD 61\\RD61Git\\Halcon");
                hVerificationProcedure = new HDevProcedure(VerificationProcedure);
                hLabelProcedure = new HDevProcedure(LabelProcedure);
                mLabelProcedureCall = new HDevProcedureCall(hLabelProcedure);
                mVerificationProcedureCall = new HDevProcedureCall(hVerificationProcedure);
                Log.I($"Halcon Engine Objects Created");
            }
            catch (HDevEngineException hengex)
            {
                Log.E($"Error in Creating the HdevEngine Exception Message:{hengex.Message}");
            }
            catch (Exception ex)
            {
                Log.E($"Eception Throw by EngineConstructor in CameraManager Exception Message: {ex.Message}");
            }
        }
        #endregion
        #region Nofiy Propertry Implimentation
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region Property Subscribtion Methods
        public void OnPlcPropertyChanged(object sender, PropertyChangedEventArgs e )
        {
            if(e.PropertyName == "StartCameraInspection")
            {
                if(PlcCommsManager.Instance.StartCameraInspection)
                {
                    StartCameraInspection = true;
                }
                else
                {
                    StartCameraInspection = false;
                } 
            }
            if(e.PropertyName == "LabelComplete")
            {
                if (!PlcCommsManager.Instance.LabelComplete)
                {
                    LabelComplete = false;
                }
                
            }
            if (e.PropertyName == "VerificationComplete")
            {
                if (!PlcCommsManager.Instance.VerificationComplete)
                {
                    VerificationComplete = false;
                }
                
            }
            if (e.PropertyName == "StartLabelCameraInspection")
            {
                if (PlcCommsManager.Instance.StartLabelCameraInspection)
                {
                    StartLabelCameraInspection = PlcCommsManager.Instance.StartLabelCameraInspection;
                }
                else
                {
                    StartLabelCameraInspection = false;
                }
            }
            if (e.PropertyName == "StartVerificationCameraInspection")
            {
                if (PlcCommsManager.Instance.StartVerificationCameraInspection)
                {
                    StartVerificationCameraInspection = PlcCommsManager.Instance.StartVerificationCameraInspection;
                }
                else
                {
                    StartVerificationCameraInspection = false;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //Free Halcon Object
                    hFrameGrabberLabelCamera.Dispose();
                    hFrameGrabberVerificationCamera.Dispose();
                }

                

                disposedValue = true;
            }
        }

        
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            
        }
        #endregion
        #endregion
    }
}
