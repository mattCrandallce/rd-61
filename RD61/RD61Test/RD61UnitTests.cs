﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RD61;
using RD61.Base;
using RD61.PLC;
using System.Threading;

namespace RD61Test
{
    [TestClass]
    public class RD61UnitTests
    {
        [TestMethod]
        public void PLC_Connection()
        {
            bool expected = true;

            string _testAMSID = "10.100.100.2.1.1";
            int _testPort = 851;
            int _testReConTime = 5;
            var plc = PlcCommsManager.Instance;
            plc.InitiatePlcComms(_testAMSID, _testPort, _testReConTime);
            Assert.AreEqual(expected, PlcCommsManager.Instance.PlcRunning);
        }
        [TestMethod]
        public void PLC_Property_Change()
        {
            //set up 
            bool expected = true;
            string _testAMSID = "10.100.100.2.1.1";
            int _testPort = 851;
            int _testReConTime = 5;
            var plc = PlcCommsManager.Instance;
            plc.InitiatePlcComms(_testAMSID, _testPort, _testReConTime);
            plc.plc.vars.StartCameraInspection = true;
            //Test
            Assert.AreEqual(expected, plc.StartCameraInspection);
        }
        [TestMethod]
        public void PLC_WriteTest()
        {
            bool expected = true;
            
            string _testAMSID = "10.100.100.2.1.1";
            int _testPort = 851;
            int _testReConTime = 5;
            PlcCommsManager.Instance.InitiatePlcComms(_testAMSID, _testPort, _testReConTime);
            //test
            PlcCommsManager.Instance.plc.SetBoolValue(PlcCommsManager.Instance.plc.vars.bstartCameraInspection, true);
            //ensure that the plc notify task has enough time to catch up
            Thread.Sleep(1000);
            bool actual = PlcCommsManager.Instance.StartCameraInspection;
            Assert.AreEqual(expected, actual);

        }
        [TestMethod]
        public void Camera_StartTask()
        {
            var ParCamera = CameraManagerParallel.Instance;
            Thread.Sleep(100);
            Assert.AreEqual(true, CameraManagerParallel.Instance.VerificationCameraOnline);
        }
        [TestMethod]
        public void Camera_EndTask()
        {
            var ParCamera = CameraManagerParallel.Instance;
            Thread.Sleep(5000);
            ParCamera.stopBothCameras();
            Assert.AreEqual(false, CameraManagerParallel.Instance.VerificationCameraOnline);

        }
        
    }
}
