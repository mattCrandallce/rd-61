﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using RD61;
using RD61.CommonData;
using NLog;


namespace RD61.PLC
{
    public class PlcCommsManager: INotifyPropertyChanged
    {

        public static PlcCommsManager Instance { get; } = new PlcCommsManager();

       protected ILog Log { get; }

        public PlcCommsManager()
        {
            Log = new Log("PlcCommsManager");
        }
        
        public PlcComms plc = null;

        /// <summary>
        /// The time interval after which the app will try and reconnect to the PLC after a heartbeat failure
        /// </summary>
        public int PlcReconnectTime { get; set; }


        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region INotify Properties

      
        private bool _plcRunning;
        /// <summary>
        /// Notification that the service is connected to the PLC
        /// </summary>
        public bool PlcRunning
        {
            get { return _plcRunning; }
            set
            {
                if (_plcRunning == value) return;
                _plcRunning = value;
                if (_plcRunning)
                {
                    Log.I("Plc Comms are active");
                }
                else
                {
                    Log.I("Plc Comms are not active");
                }
                OnPropertyChanged();
            }
        }


        private bool _startCameraInspection;

        public bool StartCameraInspection
        {
            get { return _startCameraInspection; }
            set
            {
                if (value == _startCameraInspection) return;
                _startCameraInspection = value;
                OnPropertyChanged();
            }
        }
        private bool _startVerificationCamerInspection;

        public bool StartVerificationCameraInspection
        {
            get { return _startVerificationCamerInspection; }
            set
            {
                if (_startVerificationCamerInspection == value) return;
                _startVerificationCamerInspection = value;
                OnPropertyChanged();
            }
        }

        private bool _startLabelCamerInspection;

        public bool StartLabelCameraInspection
        {
            get { return _startLabelCamerInspection; }
            set
            {
                if (_startLabelCamerInspection == value) return;
                _startLabelCamerInspection = value;
                OnPropertyChanged();
            }
        }
        private bool _verificationComplete;

        public bool VerificationComplete
        {
            get { return _verificationComplete; }
            set
            {
                if (_verificationComplete != value)
                {
                    _verificationComplete = value;
                    OnPropertyChanged();
                };
            }
        }
        private bool _labelComplete;

        public bool LabelComplete
        {
            get { return _labelComplete; }
            set
            {
                if (_labelComplete != value)
                {
                    _labelComplete = value;
                    OnPropertyChanged();
                };
            }
        }



        #endregion

        #region Public Methods

        #region InitiatePlcComms

        /// <summary>
        /// Clean the route
        /// </summary>
        /// <param name="amsId"></param>
        /// <param name="amsPort"></param>
        public void InitiatePlcComms(string amsId, int amsPort, int reconnectTime)
        {
            PlcReconnectTime = reconnectTime*1000;
            plc = new PlcComms();
            plc.AmsId = amsId;
            plc.AmsPort = amsPort;
            plc.PropertyChanged += new PropertyChangedEventHandler(OnPlcPropertyChanged);
            //Log.I("Cleaning AMS Route for PLC Comms...");
            plc.CleanAmsRoute();
        }

        #endregion
        public void WriteCameraResultsLabel(LabelCameraData LabelData)
        {
            plc.WriteCameraResultsLabel(LabelData);
        }
        public void WriteCameraResultsVerification(VerificationCameraData VerificationData)
        {
            plc.WriteCameraResultsVerification(VerificationData);
        }
        public void WriteCameraResults(LabelCameraData LabelData,VerificationCameraData VerificationData)
        {
             plc.WriteCameraResults(LabelData, VerificationData);
        }

        #region Shutdown

        /// <summary>
        /// Call this method to safely shut down the PLC Comms
        /// </summary>
        public void Shutdown()
        {
            if (plc != null)
            {
                plc.PropertyChanged -= new PropertyChangedEventHandler(OnPlcPropertyChanged);
                plc.Dispose();
            }
        }

        #endregion

        #region Talk to stuff

        

        public short GetShortValue(int varHandle)
        {
            short _data = plc.GetShortValue(varHandle);
            return _data;
        }
       
        

        #endregion

        #endregion

        #region Plc Change Notifications

        /// <summary>
        /// PLC properties changed. Add to this to communicate to the application core
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPlcPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "AmsRouteCleaned" && plc.AmsRouteCleaned)
            {

                ConnectToPlc();
            }
            if (e.PropertyName == "CommsRunning")
            {
                if (PlcRunning && !plc.CommsRunning)
                {
                    PlcRunning = false;
                    ConnectToPlc();
                }
                PlcRunning = plc.CommsRunning;
                
            }
            if (e.PropertyName == "StartCameraInspection")
            {
               
               if (plc.StartCameraInspection)
                {
                    StartCameraInspection = plc.StartCameraInspection;
                }
                else
                {
                    StartCameraInspection = false;
                }
            }
            if(e.PropertyName == "LabelComplete")
            {
                if (plc.LabelComplete)
                {
                    LabelComplete = true;
                }
                else
                {
                    LabelComplete = false;
                }
            }
            if(e.PropertyName == "VerificationComplete")
            {
                if (plc.VerificationComplete)
                {
                    VerificationComplete = true;
                }
                else
                {
                    VerificationComplete = false;
                }
            }
            if (e.PropertyName == "StartLabelCameraInspection")
            {
                if (plc.StartLabelCameraInspection)
                {
                    StartLabelCameraInspection = plc.StartLabelCameraInspection;
                }
                else
                {
                    StartLabelCameraInspection = false;
                }
            }
            if (e.PropertyName == "StartVerificationCameraInspection")
            {
                if (plc.StartVerificationCameraInspection)
                {
                    StartVerificationCameraInspection = plc.StartVerificationCameraInspection;
                }
                else
                {
                    StartVerificationCameraInspection = false;
                }
            }
        }

        #endregion
     
        #region Try Connect

        /// <summary>
        /// Try to connect every 5s while CommsRunning is false.
        /// </summary>
        private void ConnectToPlc()
        {
            try
            {
                while (!plc.CommsRunning)
                {
                    System.Threading.Thread.Sleep(PlcReconnectTime);
                    Log.I("Plc is connecting...");
                    plc.ConnectPlc();
                }
            }
            catch (Exception ex)
            {
                Log.E($"Message from {this.ToString()},", "ConnectToPlc method ", $"Exception: {ex.Message}" );
            }
        }

        #endregion
    
    }

   
}
#endregion