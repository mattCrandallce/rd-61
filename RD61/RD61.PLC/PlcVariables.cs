﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using TwinCAT.Ads;
using System.Runtime.CompilerServices;
using RD61.CommonData;


namespace RD61.PLC
{
    public class PlcVariables : INotifyPropertyChanged, IDisposable
    {
        #region PLC Variables
        //Add new PLC variables here

        //General variables
        //[PlcVar("XI.bResetAlarms", eReadWrite.ReadWrite, typeof(bool))]
        //public int hResetAlarms;
        //[PlcVar("XI.bHeartBeat", eReadWrite.Notify, typeof(bool))]
        //public int hHeartbeat;
        [PlcVar("XI.OutfeedExternalInterfaces.bstartCameraInspection", eReadWrite.Notify, typeof(bool))]
        public int bstartCameraInspection;
        [PlcVar("XI.OutfeedExternalInterfaces.xcoord", eReadWrite.Notify, typeof(double))]
        public int LabelCameraXCoordinate;
        [PlcVar("XI.OutfeedExternalInterfaces.ycoord", eReadWrite.Notify, typeof(double))]
        public int LabelCameraYCoordinate;
        [PlcVar("XI.OutfeedExternalInterfaces.Barcode", eReadWrite.Notify, typeof(string))]
        public int VerificationCameraBarcode;
        [PlcVar("XI.OutfeedExternalInterfaces.bstartLabelCameraInspection", eReadWrite.Notify, typeof(bool))]
        public int bstartLabelCameraInspection;
        [PlcVar("XI.OutfeedExternalInterfaces.bstartVerificationCameraInspection", eReadWrite.Notify, typeof(bool))]
        public int bstartVerificationCameraInspection;
        [PlcVar("XI.OutfeedExternalInterfaces.LabelOrientation", eReadWrite.Notify, typeof(int))]
        public int LabelOrientation;
        [PlcVar("XI.OutfeedExternalInterfaces.bBoxPresent", eReadWrite.Notify, typeof(bool))]
        public int BoxPresent;
        [PlcVar("XI.OutfeedExternalInterfaces.bNegativeX", eReadWrite.Notify, typeof(bool))]
        public int NegativeX;
        [PlcVar("XI.OutfeedExternalInterfaces.bNegativeY", eReadWrite.Notify, typeof(bool))]
        public int NegativeY;
        [PlcVar("XI.OutfeedExternalInterfaces.Q1", eReadWrite.Notify, typeof(int))]
        public int Q1;
        [PlcVar("XI.OutfeedExternalInterfaces.Q4", eReadWrite.Notify, typeof(int))]
        public int Q4;
        #endregion

        #region Properties

        protected TcAdsClient adsClient { get; set; }
        

        #endregion

        #region Private var declarations

        private List<ExtendedPlcVarInfo> plcHandles;
        public List<ExtendedPlcVarInfo> notifyHandles;
        ILog Log;

        #endregion

        #region Constructors

        public PlcVariables(TcAdsClient ads)
        {
            Log = new Log("PlcVariables");
            this.adsClient = ads;
            BuildHandleCollection();
            StartNotificationTask();
        }

        #endregion

        #region Build Collections

        /// <summary>
        /// Add all Plc vars to a collection and then create variable handles for each
        /// </summary>
        private void BuildHandleCollection()
        {
            try
            {
                plcHandles = new List<ExtendedPlcVarInfo>();
                foreach (FieldInfo item in this.GetType().GetFields())
                {
                    object[] attr = item.GetCustomAttributes(typeof(PlcVarAttribute), false);
                    if (attr.Length > 0)
                    {
                        PlcVarAttribute pa = (PlcVarAttribute)attr[0];
                        plcHandles.Add(new ExtendedPlcVarInfo(item, pa));
                    }
                }
                foreach (ExtendedPlcVarInfo item in plcHandles)
                {
                    try
                    {
                        item.Info.SetValue(this, adsClient.CreateVariableHandle(item.Attr.VarName));
                    }
                    catch (AdsException adsEx)
                    {
                        Log.E($"ADS Exception from BuildHandleCollection: {adsEx.Message}");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.E($"General Exception from BuildHandleCollection: {ex.Message}");
            }
        }

        #region Delete variable handles

        public void DeleteVariableHandles()
        {
            try
            {
                foreach (ExtendedPlcVarInfo item in plcHandles)
                {
                    adsClient?.DeleteVariableHandle((int)item.Info.GetValue(this));
                }
            }
            catch (Exception) { }
        }

        #endregion

        #endregion

        #region PLC Change Notification

        CancellationTokenSource tokenSource;
        CancellationToken token;
        Task notifyTask;
        int pollTime = 100;//ms

        private void StartNotificationTask()
        {
            try
            {
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                notifyTask = new Task(obj => NotificationTask(), token, TaskCreationOptions.LongRunning);
                notifyTask.Start();
            }
            catch (Exception ex)
            {
                Log.E($"General exception from StartNotificationTask: {ex.Message}");
            }
        }


        private void NotificationTask() //could change return type to a custom event to get out changed data (collection)
        {
            try
            {
                while (true)
                {
                    //Sleep for pollTime before reading the notification variables
                    bool cancelled = token.WaitHandle.WaitOne(pollTime);

                    //If the task has been canceled, throw the exception and stop the task
                    if (token.IsCancellationRequested)
                    {
                        //add cleanup items, if necessary
                        throw new OperationCanceledException(token);
                    }
                    else
                    {
                        //update variables
                        //Heartbeat = (bool)adsClient.ReadAny(hHeartbeat, typeof(bool));
                        StartCameraInspection = (bool)adsClient.ReadAny(bstartCameraInspection, typeof(bool));
                        StartLabelCameraInspection = (bool)adsClient.ReadAny(bstartLabelCameraInspection, typeof(bool));
                        StartVerificationCameraInspection = (bool)adsClient.ReadAny(bstartVerificationCameraInspection, typeof(bool));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.E($"General Exception from NotificationTask: {ex.Message}");
            }
        }

        #endregion

        #region INotifyPropertyChanged Implementaton

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region INotify Properties
        private bool _heartbeat;
        /// <summary>
        /// Coordinate with PLC to verify comms are still running
        /// </summary>
        public bool Heartbeat
        {
            get { return _heartbeat; }
            set
            {
                if (_heartbeat == value) return;
                _heartbeat = value;
                OnPropertyChanged();
            }
        }

        
        private bool _startCamerInspection;

        public bool StartCameraInspection
        {
            get { return _startCamerInspection; }
            set {
                if (_startCamerInspection == value) return;
                _startCamerInspection = value;
                OnPropertyChanged();
                }
        }

        private bool _startVerificationCamerInspection;

        public bool StartVerificationCameraInspection
        {
            get { return _startVerificationCamerInspection; }
            set
            {
                if (_startVerificationCamerInspection == value) return;
                _startVerificationCamerInspection = value;
                OnPropertyChanged();
            }
        }

        private bool _startLabelCamerInspection;

        public bool StartLabelCameraInspection
        {
            get { return _startLabelCamerInspection; }
            set
            {
                if (_startLabelCamerInspection == value) return;
                _startLabelCamerInspection = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #endregion




        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    DeleteVariableHandles();
                    tokenSource.Cancel();
                    try
                    {
                        notifyTask.Wait();
                    }
                    catch (AggregateException ae)
                    {
                        if (ae.InnerException is TaskCanceledException)
                        {
                            //Log.I($"plcNotifyTask canceled");
                        }
                        else
                        {
                            //Log.E($"Exception from Dispose: {ae.Message}");
                        }
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~PlcVariables() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }

    #region PLC Attribute Definition

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class PlcVarAttribute : System.Attribute
    {

        private string _varName;
        public string VarName
        {
            get { return _varName; }
        }

        private eReadWrite _direction;
        public eReadWrite Direction
        {
            get { return _direction; }
        }

        private int _cycleTime;
        public int CycleTime
        {
            get { return _cycleTime; }
        }

        private Type _plcVarType;
        public Type PlcVarType
        {
            get { return _plcVarType; }

        }

        public PlcVarAttribute(string plcVarname, eReadWrite direction, Type plcVarType, int cycleTime = 100)
        {
            _varName = plcVarname;
            _direction = direction;
            _cycleTime = cycleTime;
            _plcVarType = plcVarType;
        }


    }

    public class ExtendedPlcVarInfo
    {
        public FieldInfo Info;
        public PlcVarAttribute Attr;

        public ExtendedPlcVarInfo(FieldInfo info, PlcVarAttribute attr)
        {
            this.Info = info;
            this.Attr = attr;
        }
    }


    public enum eReadWrite
    {
        NotDefined = 0,
        ReadOnly,
        WriteOnly,
        ReadWrite,
        Notify,
        Auto
    }

    #endregion

}