﻿#define Debug //Debug or Release

using System;
using System.Linq;
using TwinCAT.Ads;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Timers;
using System.Threading.Tasks;
using RD61;
using RD61.CommonData;
using NLog;
using HalconDotNet;



namespace RD61.PLC
{
    public class PlcComms : INotifyPropertyChanged, IDisposable
    {
        #region private variable declartions

        private TcAdsClient ads = null;
        public PlcVariables vars = null;
        private System.Timers.Timer HeartbeatTimer = null;
        protected ILog Log;
        private LabelCameraData _labelData;
        private VerificationCameraData _verificationData;

        #endregion

        #region Properties

        /// <summary>
        /// The Ams ID of the Conveyor PLC
        /// </summary>
        public string AmsId { get; set; }
        /// <summary>
        /// The port for the PLC, 851 for TC3
        /// </summary>
        public int AmsPort { get; set; }
        private bool _commsRunning;

        public bool CommsRunning
        {
            get { return _commsRunning; }
            set
            {
                if (_commsRunning != value)
                {
                    _commsRunning = value;
                    OnPropertyChanged();
                }
                }
        }

        #endregion

        #region Constructors

        public PlcComms()
        {
            Log = new Log("PLCComms");
            _labelData = new LabelCameraData();
            _verificationData = new VerificationCameraData();
        }

        #endregion

        #region Cleanup the route
        /// <summary>
        /// Re-initialize the local router
        /// </summary>
        public void CleanAmsRoute()
        {
            try
            {
                AmsRouteCleaned = false;
                TcAdsClient adsClientClean = new TcAdsClient();
                adsClientClean.Connect(10000);
                adsClientClean.WriteControl(new StateInfo(AdsState.Reconfig, adsClientClean.ReadState().DeviceState));

                adsClientClean.Dispose();
                System.Threading.Thread.Sleep(2000); //wait for port to initialize
                AmsRouteCleaned = true;
            }
            catch (Exception ex)
            {
                Log.E($"Message from {this.ToString()}, Exception from CleanAmsRoute: {ex.Message}");
            }
        }

        #endregion

        #region ConnectPlc

        bool startup = true;

        /// <summary>
        /// Verify connection is possible and then connect to PLC, create variable handles
        /// and subscribe to PLC Change notifications
        /// </summary>
        public void ConnectPlc()
        {
            //HeartbeatTimer = new System.Timers.Timer();
            //HeartbeatTimer.Interval = 10000;
            //HeartbeatTimer.Elapsed += new System.Timers.ElapsedEventHandler(HeartbeatTimer_Elapsed);
            //HeartbeatTimer.AutoReset = true;

            try
            {
                if (!string.IsNullOrEmpty(AmsId) & AmsPort != 0)
                {
                    ads = new TcAdsClient();
                    AmsRouterState routerState = ads.RouterState;
                    if (routerState == AmsRouterState.Start)
                    {
                        ads.Connect(AmsId, AmsPort);
                        StateInfo info = ads.ReadState();//throws AdsErrorException if not connected or local port is closed
                        if (info.AdsState == AdsState.Run)
                        {
                            //Create new PLC Variable List
                            vars = new PlcVariables(ads);
                            vars.PropertyChanged += new PropertyChangedEventHandler(OnVarsNotification);
                            
                            //HeartbeatTimer.Start();
                            
                            CommsRunning = true;
                        }
                        else
                        {
                            Log.E("the AMS Router not in Run State Plc communications is not started");
                        }
                    }
                    else
                    {
                        Log.E("the AMS Router for Plc communications is not started");
                       
                    }
                }
                else
                {
                    throw new InvalidOperationException("AmsID and Port must be set before calling Connect on the PLC");
                }
            }
            catch (InvalidOperationException invalidException)
            {
                Log.E($"Invalid Operation Exception from ConnectPlc: {invalidException.Message}");
            }
            catch (AdsErrorException adsException)
             {
                Log.E($"ADS Exception from ConnectPlc: {adsException.Message}");
            }
            catch (Exception generalException)
            {
                Log.E($"General exception from ConnectPlc: {generalException.Message}");
            }
        }

        #endregion

        #region Set Plc Value

        public void SetBoolValue(int handle, bool set)
        {
            ads.WriteAny(handle, set);
        }

        public void SetShortValue(int handle, short value)
        {
            ads.WriteAny(handle, value);
        }
        public void SetStringValue(int handle, string value)
        {
            int[] args = new int[1];
            args[0] = value.Length;
            ads.WriteAny(handle, value, args);
        }
        #endregion

        public short GetShortValue(int varHandle)
        {
            short _value = 0;
            try
            {
                _value = (short)ads.ReadAny(varHandle, typeof(short));
            }
            catch (Exception)
            {
                throw;
            }
            return _value;
        }

        #region PLC Vars Notification

        private void OnVarsNotification(object sender, PropertyChangedEventArgs e)
        {
            //if (e.PropertyName == "Heartbeat")
            //{
            //    if (vars.Heartbeat)
            //    {
            //        HeartbeatTimer.Stop();
            //        ads.WriteAny(vars.hHeartbeat, false); 
            //        HeartbeatTimer.Start();
            //    }
            //}
            if (e.PropertyName == "StartCameraInspection")
            {
                if (vars.StartCameraInspection)
                {
                    StartCameraInspection = vars.StartCameraInspection;
                }
                else
                {
                    StartCameraInspection = false;
                }
            }
            if(e.PropertyName == "StartLabelCameraInspection")
            {
                if (vars.StartLabelCameraInspection)
                {
                    StartLabelCameraInspection = vars.StartLabelCameraInspection;
                }
                else
                {
                    StartLabelCameraInspection = false;
                }
            }
            if(e.PropertyName == "StartVerificationCameraInspection")
            {
                if (vars.StartVerificationCameraInspection)
                {
                    StartVerificationCameraInspection = vars.StartVerificationCameraInspection;
                }
                else
                {
                    StartVerificationCameraInspection = false;
                }
            }
            
        }

        #endregion

        #region Heartbeat Failure

        private void HeartbeatTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            HeartbeatTimer.Stop();
            HeartbeatTimer.Start();

#if Release
             //this.Dispose();
#endif

        }

        #endregion

        #region INotifyPropertyChanged Implementaton

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region INotify Properties

        private bool _amsRouteCleaned = false;
        /// <summary>
        /// Notifies the parent when the local AMS route has been cleaned and is now ready to connect to a PLC
        /// </summary>
        public bool AmsRouteCleaned
        {
            get { return _amsRouteCleaned; }
            set
            {
                if (_amsRouteCleaned == value) return;
                _amsRouteCleaned = value;
                OnPropertyChanged();
            }
        }

        private bool _startCameraInspection;

        public bool StartCameraInspection
        {
            get { return _startCameraInspection; }
            set
            {
                if (_startCameraInspection == value) return;
                _startCameraInspection = value;
                OnPropertyChanged();
            }
        }
        private bool _startVerificationCamerInspection;

        public bool StartVerificationCameraInspection
        {
            get { return _startVerificationCamerInspection; }
            set
            {
                if (_startVerificationCamerInspection == value) return;
                _startVerificationCamerInspection = value;
                OnPropertyChanged();
            }
        }

        private bool _startLabelCamerInspection;

        public bool StartLabelCameraInspection
        {
            get { return _startLabelCamerInspection; }
            set
            {
                if (_startLabelCamerInspection == value) return;
                _startLabelCamerInspection = value;
                OnPropertyChanged();
            }
        }
        private bool _labelComplete;

        public bool LabelComplete
        {
            get { return _labelComplete; }
            set
            {
                if (_labelComplete != value)
                {
                    _labelComplete = value;
                    OnPropertyChanged();
                }
            }
        }
        private bool _verificationComplete;

        public bool VerificationComplete
        {
            get { return _verificationComplete; }
            set
            {
                if (_verificationComplete != value)
                {
                    _verificationComplete = value;
                    OnPropertyChanged();
                }
            }
        }



        #endregion

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    HeartbeatTimer.Elapsed -= HeartbeatTimer_Elapsed;
                    if (vars != null)
                    {
                        vars.PropertyChanged -= OnVarsNotification;
                        vars.Dispose();
                        vars = null;
                    }
                    try
                    {
                        ads?.Dispose();
                        ads = null;
                    }
                    catch (Exception ex)
                    {
                        //Log.E($"Exception from DisposeAdsClient: {ex.Message}");
                    }
                    finally
                    {
                        CommsRunning = false;
                    }
                }

                disposedValue = true;
            }
        }


        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        #region PublicMethods
        #region Writing Results
        
        /// <summary>
        /// Method to write results over ads
        /// </summary>
        /// <param name="LabelData"></param>
        /// <param name="VerificationData"></param>
        public void WriteCameraResults(LabelCameraData LabelData, VerificationCameraData VerificationData)
        {
            ads.WriteAny(vars.LabelCameraXCoordinate, LabelData.x);
            ads.WriteAny(vars.LabelCameraYCoordinate, LabelData.y);
            
            //ads.WriteAny(vars.VerificationCameraBarcode, VerificationData.Barcode);
            ads.WriteAny(vars.bstartCameraInspection, false);
        }
        /// <summary>
        /// Method to Write Label Results Used by Camera Manager Parallel
        /// </summary>
        /// <param name="LabelData"></param>
        public void WriteCameraResultsLabel(LabelCameraData LabelData)
        {
            _labelData = LabelData;
            LabelComplete = true;
            InspectionComplete();
        }
        /// <summary>
        /// Method to Write Verification Results Used by Camera Manager Parallel
        /// </summary>
        /// <param name="VerificationData"></param>
        public void WriteCameraResultsVerification(VerificationCameraData VerificationData)
        {

            _verificationData = VerificationData;
            VerificationComplete = true;
            InspectionComplete();
        }
        /// <summary>
        /// Method called by both camera write methods if they have both called the method it will write the results over ads
        /// </summary>
        public void InspectionComplete()
        {
            if (LabelComplete && VerificationComplete)
            {
                Log.I("All Inspections Complete Writing PLC data");
                if (_labelData.x > 0 && _labelData.y > 0)
                {
                    ads.WriteAny(vars.LabelCameraXCoordinate, (short)_labelData.x);
                    ads.WriteAny(vars.LabelCameraYCoordinate, (short)_labelData.y);
                    ads.WriteAny(vars.NegativeX, false);
                    ads.WriteAny(vars.NegativeY, false);
                }
                else
                {
                    if (_labelData.x < 0)
                    {
                        _labelData.x = _labelData.x * -1;
                        ads.WriteAny(vars.LabelCameraXCoordinate, (short)_labelData.x);
                        ads.WriteAny(vars.LabelCameraYCoordinate, (short)_labelData.y);
                        ads.WriteAny(vars.NegativeX, true);
                    }
                    if (_labelData.y < 0)
                    {
                        _labelData.y = _labelData.y * -1;
                        ads.WriteAny(vars.LabelCameraYCoordinate, (short)_labelData.y);
                        ads.WriteAny(vars.NegativeY, true);
                    }
                }
                ads.WriteAny(vars.Q1, (short)_labelData.Quat[0].I);
                ads.WriteAny(vars.Q4, (short)_labelData.Quat[3].I);

                ads.WriteAny(vars.LabelOrientation, (short)_labelData.LabelOrientation);
                ads.WriteAny(vars.BoxPresent, _labelData.boxpresent);
                
                SetStringValue(vars.VerificationCameraBarcode, _verificationData.Barcode);
                // reset the Camera Inspection Variable ahead of the automatic polling
                StartCameraInspection = false;
                StartVerificationCameraInspection = false;
                StartLabelCameraInspection = false;
                LabelComplete = false;
                VerificationComplete = false;
                ads.WriteAny(vars.bstartCameraInspection, false);
                ads.WriteAny(vars.bstartLabelCameraInspection, false);
                ads.WriteAny(vars.bstartVerificationCameraInspection, false);
                Log.I("PLC Result Data Finished Writing");
            }
            else
            {
                Log.I("Not Ready to Write Data");
            }
        }
        #endregion
        #endregion
    }

    public static class GetPlcValue<T>
    {
        public static T GetValue(TcAdsClient ads, int varHandle, T member)
        {
            try
            {
                T _value = (T)ads.ReadAny(varHandle, typeof(T));
                return _value;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
