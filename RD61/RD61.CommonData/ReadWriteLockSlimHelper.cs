﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace RD61.CommonData
{
    public static class ReadWriteLockSlimHelper<T>
    {
        public static T GetReadWriteLock(ReaderWriterLockSlim locker,T member)
        {
            try
            {
                locker.EnterReadLock();
                return member;
            }
            finally
            {
                locker.ExitReadLock();
            }
        }  
        public static void SetReadWriteLock(ReaderWriterLockSlim locker, ref T member, T newValue)
        {
            try
            {
                locker.EnterWriteLock();
                if (!member.Equals(newValue)) member = newValue;
            }
            finally
            {
                locker.ExitWriteLock();
            }
        }
    }
}
