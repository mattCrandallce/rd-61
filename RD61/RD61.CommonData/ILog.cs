﻿

using System;

namespace RD61.CommonData
{
    public interface ILog
    {
        void T(string fmt, params object[] args);

        void T(Exception ex, string fmt, params object[] args);

        void D(string fmt, params object[] args);

        void D(Exception ex, string fmt, params object[] args);

        void I(string fmt, params object[] args);

        void I(Exception ex, string fmt, params object[] args);

        void W(string fmt, params object[] args);

        void W(Exception ex, string fmt, params object[] args);

        void E(string fmt, params object[] args);

        void E(Exception ex, string fmt, params object[] args);
    }
}