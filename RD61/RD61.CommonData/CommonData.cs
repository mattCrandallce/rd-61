﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;

namespace RD61.CommonData
{
    public class VerificationCameraData
    {
        public string Barcode { get; set; }
        public HImage VerificationImage { get; set; }
    }
    public class LabelCameraData
    {
        public double x { get; set; }
        public double y { get; set; }
        public int LabelOrientation { get; set; }
        public HImage LabelImage { get; set; }
        public bool boxpresent;
        public HTuple Quat { get; set; }
    }
}
